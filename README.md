[[_TOC_]]

An SELinux module for allowing google-authenticator TOTP MFA with SSH

# Build and install

```
sudo checkmodule -M -m -o google-authenticator-sshd.mod google-authenticator-sshd.te
sudo semodule_package -o google-authenticator-sshd.pp -m google-authenticator-sshd.mod
sudo semodule -i google-authenticator-sshd.pp
```
